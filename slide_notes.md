# Title slide

[Thank contributors, DoE]


# Really, the nucleon mass?
You might wonder why a lattice practitioner would be interested in the nucleon mass. After all, the mass of the proton and neutron are some of the most precisely determined quantities in physics. In fact, when written in MeV, the _conversion factor_ from atomic mass units to MeV actually introduces the dominant source of error. 

- more generally, we can use the lattice to access observables that are difficult or impossible to access experimentally

Notes:
- Most precise determination from neutron capture $`p +n \rightarrow d + \gamma`$ [[doi: 10.6028/jres.105.003](https://dx.doi.org/10.6028%2Fjres.105.003)]


# The sigma terms: what are they and what are they good for?

- $`q`$ q quark mass shift: comes from Feynman-Helman theorem; for the nucleon-pion sigma term, the sigma term is proportional to the LO correction to the chiral mass expression for $`M_N`$
- light quark calculations can be accessed through the lattice
- heavy quark sigma terms can be calculated perturbatively, while light quark terms can be extracted either through phenomenology or lattice QCD


# Relevance to direct dark matter searches

- spin-independent contribution adds coherently with each nucleon in the atom (eg, xenon, in the case of LUX)
  - in contrast, spin-dependent contributions average to 0 and do not add coherently

- suppressed by a factor of $`\beta^2`$
  - here $`\beta^2`$ is fairly small since the speed of incident particles would be roughly the speed of the sun as it orbits the Milky Way

- Disrepency between phenomenology and lqcd would introduce the largest uncertainty to this sigma term

Quadratic dependence on sigma term => enlarges uncertainty

Notes: 
- Lagrangian: https://arxiv.org/abs/1805.09795
- the supersymmetric partners of the Higgs, gauge bosons, etc mix into four mass eigenstates (the neutralinos)

# Contribution to neutralino-nucleus cross section uncertainty

Ellis assumed a sigma term of 65 +/- 8 MeV (large sigma term)
- Lattice estimates, however, are often as low as ~40 MeV -- the discrepency is even greater than highlighted here
- Uncertainty in 2008 was already large enough to impede the ability of direct dark matter searches (e.g., CDMS, XENON10) to constrain MSSM parameter space

Notes:
- delta_s^(p): strange spin contribution
- sigma_0: contribution to nucleon mass due to non-zero quark masses (related to sigma term)
- paper: https://arxiv.org/abs/0801.3656


# Two Paths to the sigma term
- 3-point functions also have disconnected diagrams
- baryon 2-point function is noisy but not as difficult to fit as a 3-point function


# Previous work
- On the left is the FLAG average for the nucleon sigma term
    - We see that most of the results tend to converge around 40 MeV, particularly if we concentrate on the green values (which FLAG has determined have better control over systematics)
    - However, most of the phenomenological results (bottom blue) converge around 60 MeV
    - Therefore there appears to be tension between the results from phenomenology and the lattice

- However, since FLAG 2019 has been compiled, there have been two major additions:
    - First, BMW announced their result, which found a sigma term largely in agreement in other lattice results 
    - Second, Gupta et al announced their result, which found a sigma term in agreement with phenomenology. 
    - So we'd like to help resolve this tension between lattice and phenomenology or perhaps instead between lattice results
    
Notes:    
- Gupta attributes their result to better accounting of the contamination from excited states


# Project objectives
- [briefly summarize]
- [thank MILC for their gauge configurations]
- [show new data]
- Shoutout to the MILC cow


# N correlator fits
[talk about effective mass plot, stability plot]

# Fit strategy: mass formulae

- We use lam_chi = 4 pi Fpi as an approximation to the chiral breaking scale
- Expansion in eps_pi
- We don't fit $`M_N`$ directly 
    - even though we've finished our scale setting, scale setting introduces strong correlations between lattice ensembles which would otherwise be uncorrelated
    - we therefore extrapolate the quantity $`M_N/\Lambda_\chi`$, which is dimensionless
- Terms in green come from the expansion of $`1/\Lambda_\chi`$
    - Notice that the LECs introduced by the expansion can be grouped with other LECs when fitting
    - There are therefore no additional LECs that need to be fit, but there are a few extra logs

Notes:
- delta contribution is negative ($`\sim m_\pi^3`$); if we include it, then the $`\sim m_\pi^2`$ term becomes more postive, and the $`m_\pi^4`$ term must necessarily be positive; further, since the sigma term is proportional to $`\sim m_\pi^2`$ at LO, the sigma term will also become more positive
- delta nucleon mass splitting 290 MeV ~ 2 $`m_\pi`$ 
    - therefore it is another mass scale in the theory
    - as it so happens, $`g_{\pi N \Delta}`$ is a large coupling
    - at large $`N_c`$, the nucleon and delta become degenerate -- delta is necessary in eft; also delta is stable in this limit
    - for some of our ensembles/pion masses, delta is stable
    - as pion mass is increased, mass splitting becomes smaller


# $`M_N/\Lambda_\chi`$ extrapolations
- Here we have an example extrapolation of $`M_N/\Lambda_\chi`$
- Looking at the plot on the left, if we try to trace the extrapolation for each lattice spacing, we see that these extrapolations cross over regularly
    - Thus either the continuum extrapolation oscillates in a very complicated manner, or
    - As suggested by Occam's razor, the continuum extrapolation is flat
- The plot on the right shows that a flat extrapolation for the discretization terms reasonably describes the data

- We could also include the PDG nucleon mass as a point, which could be useful for determining the sigma term; however in practice, we find that including this point has little benefit

Note:
- specify model that's being fit


# Expansion of $`\sigma_{N\pi}`$
- Now that we have an extrapolation for $`M_N/\Lambda_\chi`$:
  - We have worked out a novel expression for the nucleon sigma term which allows us to rewrite the expression in terms of dimensionless quantities
  - First we note that the coefficient (the terms in the first set of bracket) come from reexpressing the quark mass derivative in terms of the dimensionless quantity $`\epsilon_\pi`$. 
  - This coefficient has a small correction at $`\mathcal{O}(\epsilon_\pi^2)`$, but for now it's sufficient to think of this term as being only slightly smaller than 1, maybe about 0.9
  - More importantly, we see that in this formulation, we can separate the derivative into two expressions: one that only depends on a chiral fit of $`M_N/\Lambda_\chi`$ and a second term that only depends on a fit of $`F_\pi`$

- Here I have used asterisk to emphasize that these quantities are evaluated at the physical point; therefore we don't need to substitute in the chiral expression for $`M_N`$, for example -- it is sufficient to use the PDG value
- In the first derivative term, we observe that $`F_\pi`$ (through the chiral scale) introduces LECs; however, as mentioned previously, these LECs can be grouped with LECs from $`M_N`$ such that they don't actually need to be determined
- However, the situation with the latter derivative term is different -- here we actually need a fit to $`F_\pi`$
- That said, if we limit our analysis to $`\epsilon_\pi^2`$, we can get away with using just the FLAG value for $`l_4`$
- The most interesting observation here, however, is that the bulk of the contribution to the sigma term actually comes from the $`F_\pi`$ derivative term. Therefore fitting $`F_\pi`$  is intergral for determining the nucleon sigma term, assuming we're interested in expanding beyond $`\mathcal{O} (\epsilon_\pi^2)`$ or don't want to rely on the FLAG average for $`l_4`$
- everything in red cancels
- higher order lecs must also be determined


# $`l_4`$ dependence
- Flag reports determinations of $`l_4`$ ranging from 3.5 - 5
- Depending on the choice of $`l_4`$, we could get a value either in agreement with BMW or Gupta


# $`F_\pi`$ extrapolation
- interested in $`l_4`$
- [explain red band ($`N_f = 2 + 1`$)]
- emphasize NLO
- continuum extrapolation supports a large value of $`\sigma_{N\pi}`$


