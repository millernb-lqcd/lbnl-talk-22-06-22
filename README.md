# Talk on nucleon sigma term for LBNL (22-06-22)

## Extracting the pion-nucleon sigma term from the lattice

In this talk, I will give an overview of the nucleon sigma term and its applications, particularly its central role in direct dark matter searches. I will briefly summarize and contrast some recent work and describe how CalLat intends to calculate this quantity. Finally I will present some preliminary work from our collaboration.


Slides: 
- https://millernb-lqcd.gitlab.io/lbnl-talk-22-06-22/presentation.pdf