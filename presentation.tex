\documentclass[usenames,dvipsnames,aspectratio=169]{beamer}
\title{Extracting the pion-nucleon sigma term from the lattice}

\author[shortname]{
	\texorpdfstring{\textcolor{ProcessBlue}{Nolan Miller}}{} \and 
	G.~Bradley \and
	C.~Drischler \and
	Z.~Hall \and
	D.~Howarth \and
	C.~K\"orber \and
	M.~Lazarow \and
	H.~Monge-Camacho \and
	A.~Meyer \and
	A.~Nicholson \and
	P.~Vranas \and
	A.~Walker-Loud \and
	\emph{others}
}
					  
\date{June 22, 2022}

\titlegraphic{
	\includegraphics[height=2cm]{figs/callat_logo.png}\hspace*{1cm}~%
	\includegraphics[height=2cm]{figs/doe_sc_logo.pdf}\hspace*{1cm}~%
	\includegraphics[height=2cm]{figs/unc_logo.png} %unc_logo
}

\usepackage{braket}
\usepackage{subcaption}
\usepackage{booktabs}
\usepackage{slashed}
\usepackage{cancel}
\usepackage{tcolorbox} %boxes

\usetheme{default}
\usecolortheme{dove}
\setbeamercolor{frametitle}{fg=RoyalPurple,bg=Orchid!20}

\setbeamertemplate{navigation symbols}{%
	\usebeamerfont{footline}%
	\usebeamercolor[fg]{footline}%
	\hspace{1em}%
	\insertframenumber
}
\addtobeamertemplate{frametitle}{
   \let\insertframesubtitle\insertsectionhead}{}
\setbeamertemplate{background}
{\includegraphics[width=\paperwidth,keepaspectratio]{figs/background.jpg}}

\begin{document}

\begin{frame}
	\titlepage
\end{frame}

\begin{frame}
	\frametitle{Outline}
	\tableofcontents
\end{frame}

\section{Background}

\begin{frame}
	\frametitle{The sigma terms: what are they and what are they good for?}
	\begin{columns}
	\begin{column}{0.5\textwidth}
	The sigma terms measure the quark condensates inside the nucleon
	\begin{equation*}
		\sigma_{q} = m_{q}\braket{N | \overline q q | N}
	\end{equation*}

	These parameterize:
	\begin{itemize}
	\item quark mass contribution to $M_N$
	\item the strange content of the nucleon
	\item {\color{ProcessBlue} the spin-independent coupling to some dark matter candidates}
	\item ...and more!
	\end{itemize}

	
	\end{column}

	

	\begin{column}{0.5\textwidth}
	\begin{figure}
		\includegraphics[width=0.8\textwidth]{./figs/lux.jpg}
		\caption*{Large Underground Xenon experiment}
	\end{figure}
	\end{column}
	\end{columns}

	\vspace{\baselineskip}
	(See also Alarc\'on (2022) [\href{https://arxiv.org/abs/2205.01108}{arXiv:2205.01108}])
\end{frame}


\begin{frame}
	\frametitle{Quark mass contribution to nucleon mass}

	Nucleon mass can be decomposed into gluonic/quark contributions using the energy-momentum tensor of QCD
	\begin{align*}
		M_N &= \frac{1}{2 M_N} \braket{N | T^{\mu}_{\phantom{\mu} \mu} | N} \\
		&= \frac{1}{2 M_N} \sum_q \left[ 
			\frac{\beta(\alpha_S)}{3\alpha_s}\braket{N | \text{Tr} G_{\mu \nu} G^{\mu \nu} | N}
			+  (1 + \gamma_{m_q}) {\color{ProcessBlue}\braket{N |  m_q \overline q q | N} }
		\right]
	\end{align*}

	Sigma term parameterizes the quark mass contribution

\end{frame}

\begin{frame}
	\frametitle{Strangeness of nucleon}

	\begin{columns}
	\begin{column}{0.3\textwidth}
		\begin{figure}
			\includegraphics[width=\textwidth]{./figs/hadron_innards.png}
		\end{figure}
	\end{column}

	
	\begin{column}{0.7\textwidth}
		A nucleon is more than its valence quarks!
		\begin{itemize}
			\item[$\implies$] expect \emph{some} strange quark dependence
		\end{itemize}

		\vspace{\baselineskip}
		In fact, can relate sigma terms to octet masses
		\begin{equation*}
			\sigma_{\pi N} - \frac{2 \hat m}{m_s} \sigma_{s} 
			\approx \frac{\hat m}{m_s - \hat m} \left(M_\Xi + M_\Sigma - 2 M_N \right) \approx 30 \text{ MeV}
		\end{equation*}

		\vspace{\baselineskip}
		Expect small $\sigma_s$ (Zweig rule), but many estimates for $\sigma_{\pi N}$ are large ($\sim 60$ MeV!)
	\end{column}


		
	\end{columns}

\end{frame}


\begin{frame}
	\frametitle{Relevance to direct dark matter searches}

	\begin{columns}
	\begin{column}{0.7\textwidth}

	Direct dark matter searches look for scattering of neutralino off nuclei
	\begin{itemize}
	\item interactions either spin-independent ($\sigma_{q}$) or spin-dependent ($g_A^{q}$)
	\item velocity-dependent cross sections suppressed by $\beta^2 = (v/c)^2 \sim 10^{-8}$
	\end{itemize}
	
	\vspace{\baselineskip}
	Consider the MSSM with neutralino $\chi$.

	
	\end{column}
	\begin{column}{0.30\textwidth}
		\begin{figure}
			\includegraphics[width=0.5\textwidth]{./figs/neutralino_scattering.png}
			\caption*{[\href{https://doi.org/10.1140/epjs/s11734-021-00093-1}{Thornberry}]}
		\end{figure}
	\end{column}
	\end{columns}

	\begin{equation*}
		\mathcal L_\text{MSSM} \supset c_q \frac{m_q}{\Lambda^3} \overline \chi \chi \overline q q
		\quad \implies \quad
		\sigma_{\chi N, \text{SI}} 
		= \frac{1}{\pi \Lambda^6}\frac{M_\chi^2 M_N^2}{(M_\chi + M_N)^2} \left\vert  
			\sum_q \frac{c_q}{2 m_N} {\color{ProcessBlue} \braket{N | m_q \overline q q| N}}
		\right\vert^2
	\end{equation*}

\end{frame}

\begin{frame}
	\frametitle{Contribution to neutralino-nucleus cross section uncertainty}

	\begin{figure}
		\includegraphics[width=0.7\textwidth]{./figs/ellis_cross_section_uncertainty.pdf}
		\caption*{[Ellis (2008); \href{https://arxiv.org/abs/0801.3656}{arXiv:0801.3656}]}
	\end{figure}
	

	\emph{``We plead for an experimental campaign to determine better the $\pi$-nucleon $\sigma$ term."}

\end{frame}

\begin{frame}
	\frametitle{The experimental (\& lattice) campaigns}

	\begin{figure}
		\includegraphics[width=0.6\textwidth]{./figs/alarcon_sigma_terms_comparison.pdf}
		\caption*{[Alarc\'on (2022); \href{https://arxiv.org/abs/2205.01108}{arXiv:2205.01108}]}
	\end{figure}

\end{frame}



\begin{frame}
	\frametitle{Two (lattice) paths to the sigma term}
	
	\begin{columns}
	\begin{column}{0.5\textwidth}
	The direct approach:
	\begin{itemize}
	\item generate $\sigma_{\pi N} = \hat m \braket{N | \overline u u + \overline d d | N}$ per lattice ensemble
	\item fit the 3-point function
	\item extrapolate $\sigma_{\pi N}$ to the physical point
	\end{itemize}
	\vfill
	\end{column}

	\begin{column}{0.5\textwidth}
	{\color{ProcessBlue} The Feynman-Hellman approach}:
	\begin{itemize}
	\item generate $C(t) = \braket{0 | O_N^\dagger(t) O_N(0) | 0}$
	\item fit the 2-point function
	\item extrapolate $M_N$ to the physical point
	\item calculate $\sigma_{\pi N} = \hat m \frac{\partial M_N}{\partial \hat m} \large \vert_\text{phys point}$
	\end{itemize}
	

	\end{column}
	\end{columns}
	\begin{figure}
		\begin{subfigure}{0.35\textwidth}
		\includegraphics[width=\textwidth]{./figs/connected_with_t.png}
		\caption*{(+ disconnected diagrams)}
		\end{subfigure}
		\qquad\qquad\qquad
		\begin{subfigure}{0.35\textwidth}
		\includegraphics[width=\textwidth]{./figs/2-pt_with_t.png}
		\caption*{}
		\end{subfigure}
		\caption*{[FLAG (2019); \href{https://arxiv.org/abs/1902.08191}{arXiv:1902.08191}]}
	\end{figure}
	\vfill
\end{frame}


\begin{frame}
	\frametitle{Justification for the Feynman-Hellman approach}
	\begin{tcolorbox}[standard jigsaw, opacityback=0]
	\textbf{Theorem (Feynman-Hellman)}: given a Hermitian operator $H$ depending on real parameter $\lambda$ with eigenvectors $\ket{\psi(\lambda)}$  and eigenvalues $E(\lambda)$, 
	\begin{equation*}
		\left\langle\psi(\lambda) \middle| \frac{\partial}{\partial\lambda} H(\lambda) \middle| \psi(\lambda)\right\rangle = \frac{\partial E}{\partial\lambda} \, .
	\end{equation*}
	\end{tcolorbox}

	\vspace{\baselineskip}
	The Hamiltonian of QCD contains 
	\begin{align*}
		&\mathcal H_\text{QCD} \supset m_u \overline u u + m_d \overline d d = \hat m (\overline u u + \overline d d) + \frac 12 \delta_m (\overline u u - \overline d d) \\
		\implies& 
		\left\langle N \middle| \frac{\partial}{\partial\hat m }\left[\hat m \left(\overline u u + \overline d d\right) \right] \middle| N \right\rangle = \frac{\partial M_N}{\partial\hat m}
		= \frac{\sigma_{\pi N}}{\hat m}
	\end{align*}
\end{frame}


\section{Some recent results}
\begin{frame}
	\frametitle{Previous work}
	\begin{columns}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/flag_sigma.pdf}
			\caption*{[FLAG (2021); \href{https://arxiv.org/abs/2111.09849}{arXiv:2111.09849}]}
		\end{figure}
		\end{column}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/gupta_sigma.png}
			\caption*{[Gupta (2021); \href{https://arxiv.org/abs/2105.12095}{arXiv:2105.12095}]}
		\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{BMW '20 results}
	\begin{columns}
		\begin{column}{0.50\textwidth}
		Feynman-Hellman approach:
	\begin{itemize}
		\item replace quark mass derivative with meson mass derivative
	\begin{align*}
		\sigma_{\pi N}
		&= \hat m \frac{\partial M_N}{\partial \hat m} \\
		&= \sum_{P \in \{\pi, K\}} J_{P, \pi N} \,   m_P^2 \frac{\partial M_N}{\partial m_P^2}
	\end{align*}
		\item fit $m_q = m_q(m_P^2)$ to determine Jacobian $J$
	\end{itemize}
	Ans\"atze employed:
	\begin{itemize}
		\item Pad\'e/Taylor models for nucleon
		\item Taylor-like models for $m_q = m_q(m_P^2)$
	\end{itemize}

	\end{column}
	\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/bmw_jacobian.pdf}
			\caption*{[BMW (2020); \href{https://arxiv.org/abs/2007.03319}{arXiv:2007.03319}]}
		\end{figure}
	\end{column}
\end{columns}
	
\end{frame}
	


\begin{frame}
	\frametitle{Gupta '21 results}
	\begin{columns}
		\begin{column}{0.50\textwidth}
		Direct calculation of matrix element
		\begin{itemize}
			\item four/three state fits of 2-pt/3-pt functions
			\item include pion-nucleon state contaminants 
			\item {\color{JungleGreen} value obtained closer to phenomenology} -- attribute to better control of excited states
		\end{itemize}

		\vspace{\baselineskip}

		$\sigma_{\pi N}$ sensitive to 2-pt/3-pt priors
		\begin{itemize}
			\item narrow, $\chi$PT-motivated priors $\implies \sim 60$  MeV
			\item wider, generic priors $\implies \sim 40$  MeV
		\end{itemize}

	\end{column}
	\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/gupta_sigma_term_mpi.pdf}
			\caption*{[Gupta (2021); \href{https://arxiv.org/abs/2105.12095}{arXiv:2105.12095}]}
		\end{figure}
	\end{column}
\end{columns}

\end{frame}

\section{Preliminary work}


\begin{frame}
	\frametitle{CalLat approach to $\sigma_{\pi N}$}

	Our approach: somewhere between BMW \& Gupta
	\begin{itemize}
		\item use Feynman-Hellman method
		\item {\color{ProcessBlue} include 3 physical pion mass ensembles}
		\item analytically work out the quark mass/meson mass Jacobian using $\chi$PT
		\item employ a wide range of Ans\"atz for $M_N$ (Taylor, HB$\chi$PT)
		\item data-driven: let the model average guide the final answer (no prior bias for HB$\chi$PT or Taylor models)
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{Project objectives \& lattice details}

	\begin{columns}
		\begin{column}{0.4\textwidth}
		Objectives:
		\begin{enumerate}
		\item fit correlators
		\item extrapolate masses to the phys point
		\item calculate $\sigma_{\pi N}$ via the Feynman-Hellman theorem
		\end{enumerate}
		\begin{table}[]
			\begin{tabular}{|l|l|}
			\hline
			Action         & \begin{tabular}[c]{@{}l@{}}Valence: domain-wall\\ Sea: staggered\end{tabular} \\ \hline
			$m_\pi$        & 130 - 400 MeV                                                                 \\ \hline
			$a$            & 0.06 - 0.15 fm                                                                \\ \hline
			Scale setting? & Done!                                                                         \\ \hline
			\end{tabular}
		\end{table}

		\end{column}

		\begin{column}{0.6\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{./figs/ensembles_data.png}
				%\caption*{[Gupta, 2021; \href{https://arxiv.org/abs/2105.12095}{arXiv:2105.12095}]}
			\end{figure}
			\begin{figure}
				\includegraphics[width=0.5\textwidth]{./figs/milc_cow.png}
				%\caption*{[Gupta, 2021; \href{https://arxiv.org/abs/2105.12095}{arXiv:2105.12095}]}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{$N$ correlator fits (\texttt{a09m135})}

	\begin{columns}
		\begin{column}{0.35\textwidth}
		\begin{figure}
			\includegraphics[width=1.1\textwidth]{./figs/nucleon_a09m135_eff_mass.pdf}
		\end{figure}
		\begin{align*}
			C(t) &= \sum_n A_n e^{-E_n t} \\
			\implies m_\text{eff}(t) &= \log\frac{C(t)}{C(t+1)}
		\end{align*}
		\end{column}
		\begin{column}{0.65\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/nucleon_a09m135_stability.pdf}
		\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Parameterizations for the quark mass derivative (1/2)}
	%Recall:

	%\vspace{\baselineskip}
	Multiple ways to express the Feynman-Hellman derivative:
	\begin{align*}
		\sigma_{\pi N} &= \hat m \frac{\partial M_N}{\partial \hat m}  && \text{{\color{RubineRed} Con}: requires renormalizing quark masses}\\
		&= \frac 12 m_\pi \left[1 + \mathcal O(m_\pi^2)\right] \, \frac{\partial M_N}{\partial m_\pi} && \text{{\color{RubineRed} Con}: non-trivial $\Lambda_\chi = \Lambda_\chi (m_\pi)$ dependence}\\
		&= \frac 12 \epsilon_\pi \Lambda_\chi \left[1 + \mathcal O(\epsilon_\pi^2)\right] \frac{\partial M_N}{\partial \epsilon_\pi} && \text{{\color{RubineRed} Con}: introduces extra LECs from $\Lambda_\chi$}
	\end{align*}

	where $\epsilon_\pi = m_\pi / \Lambda_\chi$ is a $\chi$PT expansion paramter

\end{frame}


\begin{frame}
	\frametitle{Parameterizations for the quark mass derivative (2/2)}
	Recast derivative in terms of $\epsilon_\pi = m_\pi / \Lambda_\chi$:
	\begin{align*}
	\sigma_{\pi N}
	&= \hat m \frac{\partial M_N}{\partial \hat m}  \\
	&= \frac 12 \epsilon_\pi \left[1 + \mathcal O(\epsilon_\pi^2)\right] \frac{\partial {\color{JungleGreen} M_N}}{\partial \epsilon_\pi} \\
	&= \frac 12 \epsilon_\pi \left[1 + \mathcal O(\epsilon_\pi^2)\right]
	\left[
		{\Lambda_\chi^* \frac{\partial \left({\color{ProcessBlue} M_N/\Lambda_\chi }\right)}{\partial \epsilon_\pi}}
		+ {\frac{M_N^*}{\Lambda_\chi^*} \frac{\partial {\color{ProcessBlue} \Lambda_\chi}}{\partial \epsilon_\pi}}
	\right]
	\end{align*}

	This gives us options for the nucleon mass extrapolation
	\begin{enumerate}
		\item Extrapolate dimensionful ${\color{JungleGreen} M_N}$
		\item Extrapolate dimensionless {\color{ProcessBlue}$M_N/\Lambda_\chi$} (but dimensionful {\color{ProcessBlue} $\Lambda_\chi$})
	\end{enumerate}

	\vspace{\baselineskip}
	We will consider $\Lambda_\chi = 4 \pi F_\pi$
\end{frame}


\begin{frame}
	\frametitle{Fit strategy: mass formulae}
	Instead of fitting $M_N$, try dimensionless $M_N/\Lambda_\chi = M_N/4 \pi F_\pi$ $\qquad (\epsilon_\pi = m_\pi /4 \pi F_\pi)$
	\begin{align*}
		\frac{M_N}{{\color{JungleGreen}4 \pi F_\pi}} = &\phantom{+}  c_0
		& \text{(const.)}\\
		&+ \left(\beta^\text{(2)}_N - {\color{JungleGreen} c_0 \overline \ell_4^r} \right) \epsilon_\pi^2 
		+ {\color{JungleGreen} c_0 \epsilon_\pi^2 \log \epsilon_\pi^2} 
		& \text{(LO)} \nonumber \\
		&- \frac{3\pi}{2} g_{\pi NN}^2  \epsilon_\pi^3
		& \text{(NLO)} \nonumber\\
		& + \left( \beta_{N}^{(4)} + {\color{JungleGreen} c_0 \left(\overline \ell_4^r\right)^2} - {\color{JungleGreen} c_0 \beta_{F}^{(4)}}  \right) \epsilon_\pi^4
		&\text{(N$^2$LO)} \\
		&\qquad - {\color{JungleGreen}\frac 14 c_0 \epsilon_\pi^4  \left(\log  \epsilon^2 \right)^2} 
		+ \left( \alpha_N^\text{(4)} - {\color{JungleGreen} c_0 \alpha_F^\text{(4)}} - {\color{JungleGreen} 2 c_0 \overline \ell_4^r} \right) \epsilon_\pi^4 \log{\epsilon_\pi^2} 
		\nonumber
	  \end{align*}
	%Some observations:
	\begin{itemize}
		\item The {\color{JungleGreen} $1/4 \pi F_\pi$} expansion doesn't \emph{require} fitting additional LECs; it only adds some log terms
		\item We'd like to push this $M_N/F_\pi$ analysis as far as possible
	\end{itemize}
\end{frame}


\begin{frame}	
	\frametitle{$M_N/F_\pi$ extrapolations}

	\begin{columns}
		\begin{column}{0.50\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{./figs/mn_vs_epspi_0.pdf}
				\caption*{Taylor $\mathcal O(\epsilon_\pi^4, a^2)$}
			\end{figure}
			\end{column}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/mn_vs_epspi_1.pdf}
			\caption*{Taylor $\mathcal O(\epsilon_\pi^4, a^2)$ + LO chiral log }
		\end{figure}
		\end{column}
	\end{columns}
	\begin{itemize}
		\item Although the extrapolationed values are similar, the slopes are not!
	\end{itemize}

\end{frame}


\begin{frame}	
	\frametitle{Expansion of $\sigma_{\pi N}$}
	%Expand $\sigma_{\pi N} = \hat m \frac{\partial M_N}{\partial \hat m} \approx m_\pi^2 \frac{\partial M_N}{\partial m_\pi^2} \approx \frac 12 \epsilon_\pi \frac{\partial M_N}{\partial \epsilon_\pi}$ where $\epsilon_\pi = m_\pi / F_\pi = m_\pi / 4 \pi F_\pi$ 
	Expand $\sigma_{\pi N} = \hat m \frac{\partial M_N}{\partial \hat m}  = \frac 12 \epsilon_\pi [1 + \mathcal O(\epsilon_\pi^2)] \frac{\partial M_N}{\partial \epsilon_\pi}$
	\begin{equation*}
	\sigma_{\pi N} = \frac 12 \epsilon_\pi \Bigg[
		1 
		+ \overbrace{\epsilon_\pi^2 \left( \frac 52 - \frac 12 \overline \ell_3 - 2 \overline \ell_4 \right)}^{\sim 12\%}
		+ \mathcal{O} \left(\epsilon_\pi^3\right)
	\Bigg]
	\Bigg[
	\overbrace{
		{\color{ProcessBlue} F_\pi^* \frac{\partial \left(M_N/F_\pi\right)}{\partial \epsilon_\pi}}
		+ {\color{JungleGreen}\frac{M_N^*}{F_\pi^*} \frac{\partial F_\pi}{\partial \epsilon_\pi}}
	}^{\frac{\partial M_N}{\partial \epsilon_\pi}}
	\Bigg]
	\end{equation*}

	\begin{align*}
	\frac 12 \epsilon_\pi {\color{ProcessBlue} F_\pi^* \frac{\partial \left(M_N/F_\pi\right)}{\partial \epsilon_\pi}} &=
	\frac 12 F_\pi^* \left[
		\left({\color{RubineRed} -2 c_0 \left(\overline \ell_4  - 1 \right)}  + 2 \beta^\text{(2)}_N \right) \epsilon_\pi^2 
		+ \mathcal{O}\left(\epsilon_\pi^3\right)
	\right] 
	\sim 10 \text{ MeV} 
	\\
	\frac 12 \epsilon_\pi {\color{JungleGreen}\frac{M_N^*}{F_\pi^*} \frac{\partial F_\pi}{\partial \epsilon_\pi}} &=
	\frac 12 M_N^* \left[ {\color{RubineRed} 2 \left(\overline \ell_4  - 1 \right)\epsilon_\pi^2 } 
	+ \mathcal{O}\left(\epsilon_\pi^3\right) \right]
	\sim 40 \text{ MeV}
	\end{align*}
	\begin{itemize}
		\item If fitting {\color{ProcessBlue} $M_N/F_\pi	$}, must also fit a {\color{JungleGreen} second term}
		\item Largest contribution comes from {\color{JungleGreen} second term}, only one LEC at $\mathcal O(\epsilon_\pi^2)$!
		
		$\implies$ $\overline \ell_4$ must be precisely determined
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{An exploratory model}
	\begin{equation*}
		\sigma_{\pi N} = \frac 12 \epsilon_\pi \Bigg[
			\overbrace{1 
			+ \epsilon_\pi^2 \left( \frac 52 - \frac 12 \overline \ell_3 - 2 \overline \ell_4 \right)}^{\color{RubineRed} J}
			+ \mathcal{O} \left(\epsilon_\pi^3\right)
		\Bigg]
		\Bigg[
			{\color{ProcessBlue} F_\pi^* \frac{\partial \left(M_N/F_\pi\right)}{\partial \epsilon_\pi}}
			+ {\color{JungleGreen}\frac{M_N^*}{F_\pi^*} \frac{\partial F_\pi}{\partial \epsilon_\pi}}
		\Bigg]
	\end{equation*}


	Predict {\color{RubineRed} $J$} from literature: ${\color{RubineRed} J} = 0.8816(36)$ [FLAG/PDG]
	\begin{align*}
		\begin{array}{lll}
			\phantom{+}\quad \frac 12 \epsilon_\pi { \color{RubineRed} J } {\color{ProcessBlue} F_\pi^* \frac{\partial \left(M_N/F_\pi\right)}{\partial \epsilon_\pi}}
			&=10.7(2.0) \text{ MeV} 
			&\quad [\text{Taylor } \mathcal O(m_\pi^4, a^2) \text{ fit}] 
			\\
			+\quad \frac 12 \epsilon_\pi { \color{RubineRed} J } {\color{JungleGreen}\frac{M_N^*}{F_\pi^*} \frac{\partial F_\pi}{\partial \epsilon_\pi}} 
			&=41.9(1.1) \text{ MeV}
			&\quad [\text{FLAG/PDG}] 
			\\
			\hline 
			\phantom{+}\quad \sigma_{\pi N}&= 52.6(2.3) \text{ MeV}
		\end{array}
		\end{align*}
	{\color{JungleGreen} Second term} purely from FLAG/PDG -- no fit required


\end{frame}


\begin{frame}	
	\frametitle{Sensitivity of $\sigma_{\pi N}$ to the $M_N/F_\pi$ fit}

	\begin{columns}
		\begin{column}{0.50\textwidth}
			\begin{figure}
				\includegraphics[width=0.9\textwidth]{./figs/mn_vs_epspi_0.pdf}
			\end{figure}
			\begin{align*} 
				\frac 12 \epsilon_\pi J F_\pi^* \frac{\partial \left(M_N/F_\pi\right)}{\partial \epsilon_\pi}
				&= 10.7(2.0) \text{ MeV} \\
				\implies \sigma_{\pi N} &= 52.6(2.3) \text{ MeV}
			\end{align*}
			\end{column}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=0.9\textwidth]{./figs/mn_vs_epspi_1.pdf}
		\end{figure}
		\begin{align*} 
			\frac 12 \epsilon_\pi J F_\pi^* \frac{\partial \left(M_N/F_\pi\right)}{\partial \epsilon_\pi}
			&= -\phantom{0} 0.9(2.0) \text{ MeV} \\
			\implies \sigma_{\pi N} &= \phantom{-} 41.0(2.4) \text{ MeV}
			\end{align*}
		\end{column}
	\end{columns}

\end{frame}


\begin{frame}
	\frametitle{Sensitivity of $\sigma_{\pi N}$ to $\overline \ell_4$}

	\begin{columns}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/flag_l4.pdf}
			\caption*{[FLAG (2021); \href{https://arxiv.org/abs/2111.09849}{arXiv:2111.09849}]}
		\end{figure}
		\end{column}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/sigma_vs_l4_mean.pdf}
			%\caption*{Blank}
		\end{figure}
		\end{column}
	\end{columns}

\end{frame}


\begin{frame}
	\frametitle{$F_\pi$ extrapolation: $\mathcal{O}(\epsilon_\pi^2)$ $\chi$PT + $\mathcal{O}(a^4)$}

	\begin{columns}
		\begin{column}{0.48\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/fpi_vs_epi.pdf}
			%\caption*{Blank}
		\end{figure}
		\end{column}
		\begin{column}{0.48\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/l_4_vs_epsa.pdf}
			%\caption*{[Gupta, 2021; \href{https://arxiv.org/abs/2105.12095}{arXiv:2105.12095}]}
		\end{figure}
		\end{column}
	\end{columns}

	\begin{align*}
		&F_\pi = F_0 \left[
			1 
			+ \epsilon_\pi^2 \left( \overline \ell_4^r\left(\mu = 4 \pi F_\pi \right) -\log \epsilon_\pi^2  \right)
			+ \mathcal{O}\left(\epsilon_\pi^4 \right)
		\right] \\
		&\text{where}\quad \overline \ell_4^r(\mu) = (4 \pi)^2 \ell_4^r(\mu) \qquad \overline \ell_4 = \overline \ell_4^r(\mu) - \log\left[\left(m_\pi^*\right)^2 / \mu^2 \right]
	\end{align*}

\end{frame}


\begin{frame}
	\frametitle{Comparing $\chi$PT terms by order}

	\begin{figure}
		\includegraphics[width=1.0\textwidth]{./figs/histogram_all.pdf}
		%\caption*{[Gupta, 2021; \href{https://arxiv.org/abs/2105.12095}{arXiv:2105.12095}]}
	\end{figure}
	Exploratory model average:
	\begin{itemize}
		\item $F_\pi$: $\chi$PT $\mathcal{O}(\epsilon_\pi^2)$  with LECs from FLAG (no fit)
		\item $M_N/F_\pi$: Taylor $\mathcal O (m_\pi^4, a^2) + \chi\text{PT} \text{ \{{\color{YellowOrange} None}, {\color{ProcessBlue} LO}, {\color{Orchid} NLO}, {\color{JungleGreen} N$^2$LO\}}} $
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Future work}
	We have considered here just one parameterization
	\begin{equation*}
		\sigma_{\pi N} = \frac 12 \epsilon_\pi \Bigg[
			1 
			+ \overbrace{\epsilon_\pi^2 \left( \frac 52 - \frac 12 \overline \ell_3 - 2 \overline \ell_4 \right)}^{\sim 12\%}
			+ \mathcal{O} \left(\epsilon_\pi^3\right)
		\Bigg]
		\Bigg[
		\overbrace{
			{\color{ProcessBlue} F_\pi^* \frac{\partial \left(M_N/F_\pi\right)}{\partial \epsilon_\pi}}
		}^{\sim \text{0--10 MeV}}
		+
		\overbrace{
			{\color{JungleGreen}\frac{M_N^*}{F_\pi^*} \frac{\partial F_\pi}{\partial \epsilon_\pi}}
		}^{\sim 40 \text{ MeV}}
		\Bigg]
		\end{equation*}

	This is just one particular method for estimating $\sigma_{\pi N}$! Other approaches to consider
	\begin{align*}
		\bullet \quad &\sigma_{\pi N} = \frac 12 m_\pi \left[1 + \mathcal O(m_\pi^2)\right] \frac{\partial M_N}{\partial m_\pi} \text{ (MeV units)} \\
		\bullet \quad&\sigma_{\pi N} =\frac 12 \epsilon_\pi \left[1 + \mathcal O(\epsilon_\pi^2) \right] \frac{\partial M_N}{\partial \epsilon_\pi} \text{ (MeV units)} \\
		\bullet \quad& \sigma_{\pi N} =\hat m \frac{\partial M_N}{\partial \hat m} \text{ (MeV units)} \\
		\bullet \quad& \text{Include contribution from $\Delta$} & \phantom{\qquad \qquad \qquad}
	\end{align*}

\end{frame}

\begin{frame}
	\frametitle{Summary}

	\begin{columns}
		\begin{column}{0.5\textwidth}
		In conclusion:
		\begin{itemize}
			\item Tension exists between phenomenology and the lattice w.r.t. $\sigma_{\pi N}$
			\item Can extract $\sigma_{\pi N}$ from a dimensionless fit of $M_N/4 \pi F_\pi $
			\item However, this requires a precise determination of the LECs associated with $F_\pi$
			\item As the $F_\pi$ term contributes $\sim 40$~MeV to $\sigma_{\pi N}$, this sets a rough lower bound on the sigma term
		\end{itemize}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{./figs/histogram_sigma.pdf}
				%\caption*{Some fig}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\section{Extra slides}
\end{document}
